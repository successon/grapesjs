import React, { Component, lazy, Suspense } from 'react';
import 'grapesjs/dist/css/grapes.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {GEditor} from 'grapesjs-react';
import SimpleBlock from './blocks/SimpleBlock';
import SimpleBlock1 from './blocks/SimpleBlock1';
import ImageBlock from './blocks/ImageBlock';
import TextBlock from './blocks/TextBlock';
import Table from './blocks/TableBlock';
import gr from 'grapesjs';




class About extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        let editor = gr.init({
            showOffsets: 1,
            noticeOnUnload: 0,
            container: '#gjs',
            //height: '100%',
            fromElement: true,
            storageManager: { type: 0 },
            styleManager : {
                sectors: [{
                    name: 'General',
                    open: false,
                    buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom']
                },{
                    name: 'Flex',
                    open: false,
                    buildProps: ['flex-direction', 'flex-wrap', 'justify-content', 'align-items', 'align-content', 'order', 'flex-basis', 'flex-grow', 'flex-shrink', 'align-self']
                },{
                    name: 'Dimension',
                    open: false,
                    buildProps: ['width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
                },{
                    name: 'Typography',
                    open: false,
                    buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-shadow'],
                },{
                    name: 'Decorations',
                    open: false,
                    buildProps: ['border-radius-c', 'background-color', 'border-radius', 'border', 'box-shadow', 'background'],
                },{
                    name: 'Extra',
                    open: false,
                    buildProps: ['transition', 'perspective', 'transform'],
                }
                ],
            },
        });


        let countNumber = 2;
        editor.BlockManager.add('testBlock', new SimpleBlock());
        editor.BlockManager.add('Block', new SimpleBlock1());
        editor.BlockManager.add('text', new TextBlock());
        editor.BlockManager.add('image', new ImageBlock());
        editor.BlockManager.add('table-block', new Table());
        editor.setComponents({
            type: 'wrapper',
            content: `<div style="text-align: center; color:rgba(255,255,255,0.75); margin-top: 645px;">- 1 -</div>`,
            classes:['panel'],
            droppable: true,
            draggable: false,
            components: [],
            removable: false,
        });
        const panelManager = editor.Panels;
        panelManager.addButton('options',{
            id: 'clear',
            label: 'Clear',
            context: 'clear',
            command(editor) {
                editor.setComponents({
                    type: 'wrapper',
                    content: `<div style="text-align: center; color:rgba(255,255,255,0.75); margin-top: 645px;">- 1 -</div>`,
                    classes:['panel'],
                    droppable: true,
                    draggable: false,
                    components: [],
                    removable: false,
                });
                countNumber = 2;
            }
        });
        panelManager.addButton('options',{
            id: 'save',
            label: 'Save',
            context: 'save',
            command(editor) {
                let opened = window.open("");
                let css = editor.getCss();
                let html = editor.getHtml();
                console.log(editor.getSelected());
                let site = "<html><head><title>MyTitle</title><style>" + css + "</style></head><body>" + html + "</body></html>";
                opened.document.writeln(site);
            }
        });
        panelManager.addButton('options',{
            id: 'addPage',
            label: 'Add Page',
            context: 'addPage',
            command(editor) {
                editor.addComponents({
                    type: 'wrapper',
                    classes:['panel', "white-color"],
                    droppable: true,
                    content: '<div style="text-align: center; color:rgba(255,255,255,0.75); margin-top: 645px;">- ' + countNumber + ' -</div>',
                    draggable: false
                });
                ++countNumber;
            }
        });
        editor.on("component:remove", model => {
            if(model.attributes.type === 'wrapper') {
                --countNumber;
            }
        });

        let menu = document.createElement("menu");
        menu.classList.add('context-menu');


        document.querySelector('iframe').contentWindow.document.lastElementChild.appendChild(menu);
        editor.DomComponents.addType('cell', {
            isComponent: el => el.tagName === 'TD',
            view: {
                events: {
                    contextmenu: 'doSmth'
                },
                doSmth(e) {
                    console.log('cell')
                    e.preventDefault();
                    e.stopPropagation();
                    let arr = ['пункт меню'];
                    let items = addMenuItems(arr, e);
                    items.forEach((item, i) => {
                        item.addEventListener('click', () => {
                            if(i == 0) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'background-color': 'green'}));
                            }
                            menu.style.display = "none";
                        })
                    })
                }
            }
        });

        editor.DomComponents.addType('default', {
            isComponent: el => el.tagName === 'DIV',
            view: {
                events: {
                    contextmenu: 'doSmth'
                },
                doSmth(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    console.log('default')
                    let arr = ['закрасить зеленым', 'убрать фон', "поднять наверх"];
                    let items = addMenuItems(arr, e);
                    items.forEach((item, i) => {
                        item.addEventListener("click", () => {
                            if(i == 0) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'background-color': 'green'}));
                            } else if(i == 1) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'background-color': ''}));
                            } else if(i == 2) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'z-index': '1'}));
                            }
                            menu.style.display = "none";
                        });

                    })
                }
            }
        });




        editor.DomComponents.addType('text', {
            view: {
                events: {
                    dblclick: 'onActive',
                    contextmenu: 'doSmth'
                },
                size: 1,
                doSmth(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    let arr = ['увелчить размер шрифта', 'уменьшить размер шрифта', 'белый шрифт', 'черный шрифт', "поднять наверх"];
                    let items = addMenuItems(arr, e);
                    items.forEach((item, i) => {
                        item.addEventListener("click", () => {
                            if(i == 0) {
                                this.size += 0.5;
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'font-size': this.size + 'em'}));
                            } else if(i == 1) {
                                this.size -= 0.5;
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'font-size': this.size + 'em'}));
                            } else if(i == 2) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {color: 'white'}));
                            } else if(i == 3) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {color: 'black'}));
                            } else if(i == 4) {
                                this.model.setStyle(Object.assign(this.model.getStyle(), {'z-index': '1'}));
                            }
                            menu.style.display = "none";
                        });

                    })
                },
                onActive(e) {
                    // We place this before stopPropagation in case of nested
                    // text components will not block the editing (#1394)
                    if (this.rteEnabled || !this.model.get('editable')) {
                        return;
                    }
                    e && e.stopPropagation && e.stopPropagation();
                    const rte = this.rte;

                    if (rte) {
                        try {
                            this.activeRte = rte.enable(this, this.activeRte);
                        } catch (err) {
                            console.error(err);
                        }
                    }

                    this.rteEnabled = 1;
                    this.toggleEvents(1);
                }
            }
        })



        function addMenuItems(param, e) {
            menu.innerHTML = '';
            let arr = [];
            param.map((item, i) => {
                let name = document.createElement('li');
                name.innerHTML = item;
                menu.appendChild(name);
                arr.push(name);
                return name;
            });
            menu.style.display = "block";
            menu.style.left = (e.clientX - 40)+"px";
            menu.style.top = (e.clientY - 15)+"px";
            return arr;
        }


        editor.on("component:add", model => {
            if(model.attributes.type === 'table') {
                let modal = gr.editors[0].Modal;
                let conteiner = document.createElement('div');
                let table = '<table><tr><td>Количество рядков</td><td><input type="text" id = "row"></td></tr><tr><td>Количество столбцов</td><td><input type="text" id="cell"></td></tr></table>';
                const button = document.createElement('button');
                button.innerHTML = 'OK';
                button.onclick = () => {

                    let components = model.get("components");
                    //components.add([]);
                    let columns = document.querySelector(`#cell`).value;
                    let rows = document.querySelector(`#row`).value;
                    //let table = document.querySelector('iframe').contentWindow.document.getElementById(model.ccid);
                    //table.style.borderCollapse = 'collapse';
                    model.setStyle(Object.assign(model.getStyle(), {'border-collapse': 'collapse'}));
                    if (components.length) {
                        const rowsToAdd = [];

                        while (rows--) {
                            const columnsToAdd = [];
                            let clm = columns;

                            while (clm--) {
                                columnsToAdd.push({
                                    type: 'cell',
                                    style: {
                                        width: "50px",
                                        height: "50px",
                                        border: "1px solid black"
                                    },
                                    resizable: true,
                                    copyable: false,
                                    draggable: ['table'],
                                    removable: false
                                });
                            }

                            rowsToAdd.push({
                                type: 'row',
                                classes: ['row'],
                                components: columnsToAdd
                            });
                        }

                        components.add(rowsToAdd);
                    }

                    modal.close();
                }
                conteiner.innerHTML = table;
                conteiner.appendChild(button)
                modal.open({
                    title: 'Таблица',
                    content: conteiner
                })
            }
        });
        editor.on("component:selected", model => {

            //menu.style.display = "none";
        });
        const commands = editor.Commands;
        commands.extend('core:component-drag', {
            onEnd(...args) {
                const { editor, opts, id } = this;
                const { onEnd } = opts;
                onEnd && onEnd(...args);
                let style = opts.target.getStyle();
                let parent = opts.target.parent().getStyle();
                if(parseFloat(style.left) < 0 || parseFloat(style.top) < 0 || parseFloat(style.left) > parseFloat(parent.width) || parseFloat(style.top) > parseFloat(parent.height)) {
                    opts.target.setStyle(Object.assign(opts.target.getStyle(), {left: '0px', top: '0px'}));
                }
                console.log(style.top, parent.height)
                editor.stopCommand(id);
                this.hideGuidesInfo();
            }
        })
        editor.on('run:core:component-drag', (model, a) => {
            if(a.target.attributes.type == 'default') {
                a.target.setStyle(Object.assign(a.target.getStyle(), {position: 'relative'}))
            }
        });

        return (
                <div>
                    {/*<GEditor blocks={[
                    new S(),
                    new I(),
                    new T(),
                    new Table()
                ]} components={[
                    new Start()
                ]} />*/}
                </div>
        );
    }
}

export default About;