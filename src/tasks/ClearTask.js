import ImageBlock from "../blocks/ImageBlock";

class ClearTask {
    constructor(countNumber) {
        this.countNumber = countNumber
    }
    id = 'clear';
    label = 'Clear';
    context = 'clear';
    command(editor) {
        editor.setComponents({
            type: 'wrapper',
            content: `<div style="text-align: center; color:rgba(255,255,255,0.75); margin-top: 645px;">- 1 -</div>`,
            classes:['panel'],
            droppable: true,
            draggable: false,
            components: [],
            removable: false,
        });
        this.countNumber = 2;
    }
}

export default ClearTask;