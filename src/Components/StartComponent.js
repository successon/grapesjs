import {GEditor, GComponent} from 'grapesjs-react';
import React, { Component, lazy, Suspense } from 'react';
import gr from 'grapesjs';

class StartComponent extends GComponent {

    constructor() {
        super('table');
    }
    events = {
        dblclick: 'onActive'
    }
    onActive() {
        let modal = gr.editors[0].Modal;
        console.log(modal);
        modal.open({
            title: 'Таблица',
            content: '<table><tr><td>Количество рядков</td><td><input type="text" id = "row"></td></tr><tr><td>Количество столбцов</td><td><input type="text" id="cell"></td></tr></table><button onclick="console.log(em); this.row = document.getElementById(`row`).value; this.cell = document.getElementById(`cell`).value">OK</button>'
        })
    }
}
export default StartComponent;