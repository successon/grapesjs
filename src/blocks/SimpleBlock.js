import {GEditor, GBlock} from 'grapesjs-react';

class SimpleBlock extends GBlock {
    constructor() {
        super('testBlock', 'Text block');
    }
    attributes = { class:'gjs-fonts gjs-f-b1' };
    content = {
        type: 'text',
        content: `<section>
                <h1>This is a simple title</h1>
                <p>Test block</p>
            </section>`,
        style: {
            position: 'absolute',
            top: '0px',
            left: '0px',
            textAlign: 'justify',
        },
        resizable: true,
        dmode: 'absolute',
        highlightable: false,
    }
    render = ({ el } ) => {
        el.addEventListener('click', () => alert('Do something'))
    }

}
export default SimpleBlock;