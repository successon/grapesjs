import {GEditor, GBlock} from 'grapesjs-react';
import React, { Component, lazy, Suspense } from 'react';
import StartComponent from '../Components/StartComponent';
import gr from "grapesjs";



class TableBlock extends GBlock {
    constructor() {
        super('testBlock3', 'Table');
    }
    attributes = { class: "fa fa-table" }
    content = {
        type: 'table',
        //classes: ['table'],
        style: {
            position: 'absolute',
            top: '25px',
            left: '25px',
        },
        dmode: 'absolute',
        highlightable: false,
        resizable: true,
    }

    activate = true

}
export default TableBlock;