import {GEditor, GBlock} from 'grapesjs-react';

class ImageBlock extends GBlock {
    constructor() {
        super('testBlock1', 'Image');
    }
    attributes = { class: "gjs-fonts gjs-f-image" }
    content = {
        type: 'image',
        style: {
            position: 'absolute',
            top: '25px',
            left: '25px',
        },
        dmode: 'absolute',
        highlightable: false
    }
    activate = true
}
export default ImageBlock;