import {GEditor, GBlock} from 'grapesjs-react';

class TextBlock extends GBlock {
    constructor() {
        super('testBlock2', 'Text for table');
    }
    attributes = { class: "gjs-fonts gjs-f-text" }
    content = {
        type: 'text',
        content: `This is a simple title`,
        style: {
            position: 'absolute',
        },
        highlightable: false
    }
}
export default TextBlock;