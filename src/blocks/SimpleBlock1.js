import {GEditor, GBlock} from 'grapesjs-react';

class SimpleBlock1 extends GBlock {
    constructor() {
        super('Block', 'Block container');
    }
    attributes = { class:'gjs-fonts gjs-f-b1' };
    content = {
        type: 'default',
        content: `<div>

            </div>`,
        style: {
            position: 'relative',
            width: 'auto',
            height: 'auto',
            'min-width': '100px',
            'min-height': '100px',
            display: 'inline-block',
            border: '1px solid black',
            float: 'left',
            textAlign: 'justify',
        },

        resizable: true,
        dmode: 'absolute',
        highlightable: false,
    }
    render = ({ el } ) => {
        el.addEventListener('click', () => alert('Do something'))
    }

}
export default SimpleBlock1;