import React, { Component, lazy, Suspense } from 'react';
import './App.css';
import 'grapesjs/dist/css/grapes.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';



class App extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
                <div className='App-header'>
                    <span>Hello</span>

                    <ul className="sub-menu">
                        <li><a href="#1">Пункт 1</a></li>
                        <li><Link to="/grapesjs">GrapesJS</Link></li>
                        <li><a href="#3">Пункт 3</a></li>
                    </ul>

                    <button className='menu-button' onClick={() => {
                        document.querySelector('ul').classList.toggle('open-menu');
                    }}><i className="fa fa-align-justify"></i></button>
                </div>
            </div>
        );
    }
}

export default App;
